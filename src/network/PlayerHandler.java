package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static network.Commands.COMMAND_DISCONNECT;

public class PlayerHandler extends Thread {

    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private CommandDispatcher dispatcher;
    private int score;

    public PlayerHandler(Socket socket) throws IOException {
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
    }

    public void run() {
        try {
            String command = in.readLine();
            while (!COMMAND_DISCONNECT.equals(command)) {
                dispatcher.dispatch(command);
                command = in.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setNumber(int number) {
        sendCommand(Commands.COMMAND_SET_PLAYER_NUMBER + " " + number);
    }

    public void setCommandDispatcher(CommandDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    public void scoreUp() {
        score++;
    }

    public int getScore() {
        return score;
    }

    public void sendCommand(String command) {
        out.println(command);
    }

    @Override
    public void interrupt() {
        super.interrupt();
        try {
            socket.close();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        out.close();
    }

}
