package network;

import game.Game;

import java.io.IOException;
import java.net.ServerSocket;

public class Server {


    public static void main(String[] args) {
        Server server = new Server();
        server.start(4444);
    }

    public void start(int port) {
        try {
            ServerSocket socket = new ServerSocket(port);
            while (!socket.isClosed()) {
                Game game = new Game();
                for (int i = 0; i < Game.PLAYERS_NUMBER; i++) {
                    game.appendPlayer(
                            new PlayerHandler(socket.accept())
                    );
                }
                game.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
