package network;

public class Commands {
    public static final String COMMAND_SET_PLAYER_NUMBER = "/setPlayerNumber";
    public static final String COMMAND_START = "/start";
    public static final String COMMAND_MOVE_PLAYER = "/movePlayer";
    public static final String COMMAND_MOVE_BALL = "/moveBall";
    public static final String COMMAND_SCORE_UP = "/scoreUp";
    public static final String COMMAND_FINISH = "/finish";
    public static final String COMMAND_DISCONNECT = "/disconnect";
}
