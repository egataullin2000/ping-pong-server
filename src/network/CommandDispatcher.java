package network;

import game.Game;
import model.Direction;

import java.awt.*;
import java.util.Scanner;

import static network.Commands.COMMAND_MOVE_PLAYER;

public class CommandDispatcher {

    private Game game;

    public CommandDispatcher(Game game) {
        this.game = game;
    }

    public void dispatch(String command) {
        Scanner scanner = new Scanner(command);
        command = scanner.next();
        if (COMMAND_MOVE_PLAYER.equals(command)) {
            int playerNumber = scanner.nextInt();
            Point newPosition = game.movePlayer(
                    playerNumber, Direction.valueOf(scanner.next())
            );
            command += " " + playerNumber + " " + newPosition.x + " " + newPosition.y;
        }

        game.sendCommand(command);
    }

}
