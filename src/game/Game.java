package game;

import model.Ball;
import model.Direction;
import network.CommandDispatcher;
import network.PlayerHandler;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static network.Commands.*;

public class Game implements GameInterface {

    public static final int PLAYERS_NUMBER = 2;

    private GameField gameField = new GameField(this);
    private CommandDispatcher dispatcher = new CommandDispatcher(this);
    private Ball ball = gameField.getBall();
    private List<PlayerHandler> players = new ArrayList<>(2);
    private Thread ballMovement = new Thread(() -> {
        try {
            while (true) {
                gameField.moveBall();
                sendCommand(COMMAND_MOVE_BALL + " " + ball.getX() + " " + ball.getY());
                Thread.sleep(20);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    });

    public void appendPlayer(PlayerHandler player) {
        player.setNumber(players.size());
        player.setCommandDispatcher(dispatcher);
        players.add(player);
    }

    public Point movePlayer(int playerNumber, Direction direction) {
        return gameField.movePlayer(playerNumber, direction);
    }

    public void start() {
        onStart();
    }

    public void sendCommand(String command) {
        for (PlayerHandler player : players) player.sendCommand(command);
    }

    @Override
    public void onStart() {
        for (PlayerHandler player : players) {
            player.sendCommand(COMMAND_START);
            player.start();
        }
        ballMovement.start();
    }

    @Override
    public void onScoreUp(int playerNumber) {
        PlayerHandler player = players.get(playerNumber);
        player.scoreUp();
        sendCommand(COMMAND_SCORE_UP + " " + playerNumber);
        if (player.getScore() == 3) onFinish(playerNumber);
    }

    @Override
    public void onFinish(int winnerNumber) {
        sendCommand(COMMAND_FINISH + " " + winnerNumber);
        ballMovement.interrupt();
    }
}
