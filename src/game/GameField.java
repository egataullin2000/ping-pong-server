package game;

import model.Ball;
import model.Direction;
import model.Player;
import model.Zone;

import java.awt.*;

public class GameField {

    private static final int WIDTH = 400;
    private static final int HEIGHT = 400;

    private GameInterface game;
    private Zone fillZone;
    private Player[] players = new Player[2];
    private Ball ball;

    public GameField(GameInterface game) {
        super();
        this.game = game;

        fillZone = new Zone(0, HEIGHT, 0, WIDTH);

        int x, y;
        x = 0;
        y = HEIGHT / 2 - Player.HEIGHT / 2;
        players[0] = new Player(x, y);

        x = WIDTH - Player.WIDTH;
        players[1] = new Player(x, y);

        x = WIDTH / 2;
        y = HEIGHT / 2;
        ball = new Ball(x, y);
    }

    public Ball getBall() {
        return ball;
    }

    public Point movePlayer(int playerNumber, Direction direction) {
        Player player = players[playerNumber];
        player.move(direction);

        return new Point(player.getX(), player.getY());
    }

    public void moveBall() {
        if (ball.collidesLeft(fillZone)) {
            game.onScoreUp(1);
            moveBallToCenter();
        } else if (ball.collidesRight(fillZone)) {
            game.onScoreUp(0);
            moveBallToCenter();
        } else {
            if (ball.collidesVertically(fillZone)) ball.changeYDirection();
            for (Player player : players) {
                Zone playerFillZone = new Zone(
                        player.getY(),
                        player.getY() + Player.HEIGHT,
                        player.getX(),
                        player.getX() + Player.WIDTH
                );
                if (ball.collidesHorizontally(fillZone) || ball.collidesHorizontally(playerFillZone)) {
                    ball.changeXDirection();
                }
                if (ball.collidesVertically(fillZone) || ball.collidesVertically(playerFillZone)) {
                    ball.changeYDirection();
                }
            }

            ball.move();

        }
    }

    private void moveBallToCenter() {
        ball.move(WIDTH / 2, HEIGHT / 2);
    }

}
