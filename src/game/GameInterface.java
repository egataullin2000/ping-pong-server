package game;

public interface GameInterface {
    void onStart();

    void onScoreUp(int playerNumber);

    void onFinish(int winnerNumber);
}
