package model;

public class Ball {

    private static final int RADIUS = 10;

    private int x, y;
    private int dx = 1, dy = 1;

    public Ball(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void changeXDirection() {
        dx *= -1;
    }

    public void changeYDirection() {
        dy *= -1;
    }

    public void move() {
        x += dx;
        y += dy;
    }

    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean collidesLeft(Zone zone) {
        return x - RADIUS <= zone.getLeftBound() && x + RADIUS >= zone.getLeftBound();
    }

    public boolean collidesRight(Zone zone) {
        return x - RADIUS <= zone.getRightBound() && x + RADIUS >= zone.getRightBound();
    }

    public boolean collidesHorizontally(Zone zone) {
        boolean collides;

        collides = collidesLeft(zone) || collidesRight(zone);
        if (collides) {
            double topBound = y - RADIUS;
            double bottomBound = y + RADIUS;
            collides = topBound >= zone.getTopBound() && topBound <= zone.getBottomBound();
            collides = collides || bottomBound >= zone.getTopBound() && bottomBound <= zone.getBottomBound();
        }

        return collides;
    }

    public boolean collidesVertically(Zone zone) {
        boolean collides;

        double topBound = y - RADIUS;
        double bottomBound = y + RADIUS;
        collides = topBound <= zone.getTopBound() && bottomBound >= zone.getTopBound();
        collides = collides || topBound <= zone.getBottomBound() && bottomBound >= zone.getBottomBound();
        if (collides) {
            double leftBound = x - RADIUS;
            double rightBound = x + RADIUS;
            collides = leftBound >= zone.getLeftBound() && leftBound <= zone.getRightBound();
            collides = collides || rightBound >= zone.getLeftBound() && rightBound <= zone.getRightBound();
        }

        return collides;
    }

}
