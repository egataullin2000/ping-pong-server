package model;

public class Zone {

    private double topBound, bottomBound;
    private double leftBound, rightBound;

    public Zone(double topBound, double bottomBound, double leftBound, double rightBound) {
        this.topBound = topBound;
        this.bottomBound = bottomBound;
        this.leftBound = leftBound;
        this.rightBound = rightBound;
    }

    public double getTopBound() {
        return topBound;
    }

    public double getBottomBound() {
        return bottomBound;
    }

    public double getLeftBound() {
        return leftBound;
    }

    public double getRightBound() {
        return rightBound;
    }

}
