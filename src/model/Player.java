package model;

public class Player {

    public static final int WIDTH = 20;
    public static final int HEIGHT = 150;
    private static final int SPEED = 5;

    private int x, y;

    public Player(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void move(Direction direction) {
        if (direction == Direction.UP) {
            y -= SPEED;
        } else if (direction == Direction.DOWN) {
            y += SPEED;
        }
    }

}
