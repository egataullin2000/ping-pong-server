package network;

import game.Game;
import model.Direction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CommandDispatcherTest {

    @InjectMocks
    private Game game = mock(Game.class);

    @Before
    public void before() {
        when(game.movePlayer(0, Direction.UP))
                .thenReturn(new Point(0, 0));
    }

    @Test
    public void test() {
        String command = "/movePlayer 0 UP";
        CommandDispatcher dispatcher = new CommandDispatcher(game);
        assertDoesNotThrow(() -> doThrow(IllegalArgumentException.class)
                .when(game)
                .sendCommand(command)
        );
        dispatcher.dispatch(command);
    }

}