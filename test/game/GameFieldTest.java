package game;

import model.Direction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class GameFieldTest {

    private Game game = mock(Game.class);
    private GameField gameField = new GameField(game);

    @Test
    public void testPlayerMovement() {
        assertDoesNotThrow(() -> gameField.movePlayer(0, Direction.UP));
    }

    @Test
    public void testBallMovement() {
        assertDoesNotThrow(gameField::moveBall);
    }

}