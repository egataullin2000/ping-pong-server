package game;

import network.PlayerHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {

    @InjectMocks
    private Game game = spy(Game.class);
    private PlayerHandler[] players;

    @Before
    public void before() {
        players = new PlayerHandler[2];
        for (int i = 0; i < 2; i++) {
            players[i] = mock(PlayerHandler.class);
            doCallRealMethod()
                    .when(players[i])
                    .scoreUp();
            doCallRealMethod()
                    .when(players[i])
                    .getScore();
        }
    }

    @Test
    public void test() {
        assertDoesNotThrow(() -> doThrow(new IllegalStateException("Wrong winner"))
                .when(game)
                .onFinish(1)
        );
        doCallRealMethod().when(game).onScoreUp(0);
        for (PlayerHandler player : players) game.appendPlayer(player);
        for (int i = 0; i < 3; i++) {
            game.onScoreUp(0);
        }
    }

}